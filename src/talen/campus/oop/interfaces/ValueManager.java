package talen.campus.oop.interfaces;

public class ValueManager {
	
	private Lookup lookup;

	public ValueManager(Lookup lookup) {
		this.lookup = lookup;
	}
	
	public void processValues(String[] names) {
		for(int i = 0; i<names.length; i++) {
			Object value = this.lookup.find(names[i]);
			if(value != null) {
				System.out.println("Process : " + value.toString());
			}
		}
	}

}
