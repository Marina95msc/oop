package talen.campus.oop.abstracts;

public abstract class Figura {
	
	protected String color;

	public Figura(String color) {
		this.color = color;
	}
	
	public abstract double calcularArea();
}
