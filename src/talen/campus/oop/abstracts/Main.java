package talen.campus.oop.abstracts;

public class Main {

	public static void main(String[] args) {
		// new Figura("Verde");
		
		Figura figura = new Cuadrado("Rojo", 5.6);
		imprimirDatosFigura(figura);
		
		figura = new Triangulo("Amarillo", 4.5, 4);
		imprimirDatosFigura(figura);
	}

	private static void imprimirDatosFigura(Figura figura) {
		System.out.println(figura.toString());
		System.out.printf("Area: %.2f\n", figura.calcularArea());
	}

}
