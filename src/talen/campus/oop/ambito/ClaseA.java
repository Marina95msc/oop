package talen.campus.oop.ambito;

public class ClaseA {
	
	public void metodo() {
		System.out.println("Clase A");
	}
	
	public void echo(String str) { 
		System.out.println(str);
	}
	
	public void echo(Integer entero) { 
		System.out.println(entero);
	}
}
