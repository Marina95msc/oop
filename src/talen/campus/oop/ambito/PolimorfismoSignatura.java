package talen.campus.oop.ambito;

public class PolimorfismoSignatura {

	public static void main(String[] args) {
		ClaseA claseA = new ClaseA();
		claseA.echo("echo");
		claseA.echo(3);
		
		ClaseC claseC = new ClaseC();
		claseC.echo("echo2");
		claseC.echo(3453);
		claseC.echo('c');
	}

}
